package Encoder;

import Encoder.crypt.AES;
import Encoder.crypt.HMAC;
import Encoder.io.Reader;
import Encoder.io.Writer;
import Encoder.utils.Format;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class Encoder {

    private static String mFileName;
    private static String mTitle;

    /**
     * Initialization
     * @param in input stream
     * @param out output stream
     * @param password password
     * @param filename filename
     * @param title title
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
     public static void initialize(InputStream in, OutputStream out, String password, String filename, String title)
     throws InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException,
            NoSuchAlgorithmException, NoSuchPaddingException {
        mFileName = filename;
        mTitle = title;

        Reader.init(in);
        Writer.init(out);
        HMAC.init(password);
        AES.init(AES.ENCODE, password);
    }

    /**
     * Writes header
     * @throws IOException
     */
    public static void updateHeader()
    throws IOException {
        // Header: version + iv + aes salt + kdf salt
        byte[] head = Format.concatBytes(Format.Ascii.bytes(Config.VERSION), AES.iv(), AES.saltAES(), AES.saltKDF());

        // Sanity
        byte[] sane = Format.Ascii.bytes(Config.SANITY);

        // Filename
        byte[] filename = Format.UTF8.bytes(mFileName);
        byte szFilename = (byte) filename.length;
        filename = Format.concatBytes(Format.Byte.bytes(szFilename), filename);

        // Title
        byte[] title = Format.UTF8.bytes(mTitle);
        byte szTitle = (byte) title.length;
        title = Format.concatBytes(Format.Byte.bytes(szTitle), title);

        // Write: Header + AES[Sanity + HMAC Salt + Filename + Title]
        byte[] extHeader = AES.update(Format.concatBytes(sane, HMAC.salt(), filename, title), AES.saltAES());
        short szExtHead = (short) extHeader.length;
        Writer.writeChunk(Format.concatBytes(head, Format.Short.bytes(szExtHead), extHeader));
    }

    /**
     * Writes data
     * @return length of read bytes
     * @throws IOException
     */
    public static int updateData()
    throws IOException {
        // Encrypt document
        byte[] data = new byte[Config.SZ_CHUNK];
        int readBytes = Reader.readChunk(Config.SZ_CHUNK, data);
        if (0 < readBytes) {
            byte[] buffer = new byte[readBytes];
            System.arraycopy(data, 0, buffer, 0, readBytes);
            HMAC.update(buffer);
            Writer.writeChunk(AES.update(buffer, AES.saltAES()));
        }
        return readBytes;
    }

    /**
     * Writes HMAC
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     */
    public static void updateHMAC()
    throws IllegalBlockSizeException, BadPaddingException, IOException {
        // HMAC
        byte[] hmac = AES.update(HMAC.finale(), AES.saltAES());
        Writer.writeChunk(Format.concatBytes(hmac, AES.finale()));
    }

    /**
     * Closes input and output streams
     * @throws IOException
     */
    public static void close()
    throws IOException {
        Reader.close();
        Writer.close();
    }
}