package Encoder.io;

import java.io.IOException;
import java.io.OutputStream;

public class Writer {

    private static OutputStream mOS;

    /**
     * Initialization
     * @param os output stream
     */
    public static void init(OutputStream os) {
        mOS = os;
    }

    /**
     * Writes chunk of data to output stream
     * @param data Data to write
     * @throws IOException
     */
    public static void writeChunk(byte[] data)
    throws IOException {
        writeChunk(data, data.length);
    }

    /**
     * Writes chunk of data to output stream
     * @param data Data to write
     * @throws IOException
     */
    public static void writeChunk(byte[] data, int length)
    throws IOException {
        mOS.write(data, 0, length);
    }

    /**
     * Closes output stream
     * @throws IOException
     */
    public static void close()
    throws IOException {
        if (null != mOS) mOS.close();
        mOS = null;
    }
}