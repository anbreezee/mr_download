package Encoder;

public interface ProgressCallback {
    public void updateProgress(int value, String description);
}
