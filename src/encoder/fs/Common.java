package Encoder.fs;

import java.io.File;
import java.io.IOException;

public class Common {

    /**
     * Deletes specified file
     * @param filename The file name
     * @throws IOException
     */
    public static void deleteFile(String filename)
    throws IOException {
        File file = new File(filename);
        if (file.exists() && !file.delete()) {
            throw new IOException("Can't delete target file");
        }
    }

    /**
     * Renames specified file to another
     * @param oldFilename Source file name
     * @param newFilename Target file name
     * @throws IOException
     */
    public static void renameFile(String oldFilename, String newFilename)
    throws IOException {
        File src = new File(oldFilename);
        File tgt = new File(newFilename);
        if (tgt.exists()) {
            throw new IOException("File exists");
        }
        if (!src.renameTo(tgt)) {
            throw new IOException("Can't rename file");
        }
    }
}