package Encoder.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Timer {

    private static class Tick {
        public String mName;
        public long mTime;
        public Tick(String name, long time) { mName = name; mTime = time; }
        public String name() { return mName; }
        public long time() { return mTime; }
    }

    private static List<Tick> ticks = new ArrayList<Tick>();

    /**
     * Appends new tick
     * @param stepName Step name
     */
    public static void tick(String stepName) {
        ticks.add(new Tick(stepName, new Date().getTime()));
    }

    /** Displays the report */
    public static void report() {
        long previous = 0;
        for (Tick tick : ticks) {
            if (0 < previous) {
                System.out.printf("%s:\t%d ms%n", tick.name(), (tick.time() - previous));
            }
            previous = tick.time();
        }
    }
}