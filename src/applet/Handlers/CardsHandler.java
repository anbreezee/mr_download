package Applet.Handlers;

import Applet.Context;
import Applet.UI.*;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.CardLayout;

public class CardsHandler {

    private Context context;                // Context

    private JPanel cards;                   // Cards
    private PanelCounter panelCounter;      // Panel: Counter
    private PanelWait panelWait;            // Panel: Wait
    private PanelNotFound panelNotFound;    // Panel: Not Found
    private PanelStart panelStart;          // Panel: Start
    private PanelDownload panelDownload;    // Panel: Download
    private PanelFinish panelFinish;        // Panel: Finish
    private PanelTryAgain panelTryAgain;    // Panel: Try Again

    /**
     * Constructor
     * @param context Context
     */
    public CardsHandler(Context context) {
        this.context = context;
    }

    /**
     * Constructs user interface
     */
    public void ConstructUI() {
        cards = new JPanel(new CardLayout());
        cards.setBackground(Color.white);

        // Card: Counter
        panelCounter = new PanelCounter(context, context);
        cards.add(panelCounter, "panelCounter");

        // Card: Wait
        panelWait = new PanelWait(context);
        cards.add(panelWait, "panelWait");

        // Card: Not Found
        panelNotFound = new PanelNotFound(context);
        cards.add(panelNotFound, "panelNotFound");

        // Card: Start
        panelStart = new PanelStart(context, context);
        cards.add(panelStart, "panelStart");

        // Card: Download
        panelDownload = new PanelDownload(context, context);
        cards.add(panelDownload, "panelDownload");

        // Card: Finish
        panelFinish = new PanelFinish(context);
        cards.add(panelFinish, "panelFinish");

        // Card: Try Again
        panelTryAgain = new PanelTryAgain(context, context);
        cards.add(panelTryAgain, "panelTryAgain");

        context.getApplet().getContentPane().add(cards);
    }

    /**
     * Shows specified card
     * @param name Panel name
     */
    public void showCard(String name) {
        CardLayout cl = (CardLayout) cards.getLayout();
        if (name.equals("counter"))       { cl.show(cards, "panelCounter");  }
        else if (name.equals("wait"))     { cl.show(cards, "panelWait");     }
        else if (name.equals("notfound")) { cl.show(cards, "panelNotFound"); }
        else if (name.equals("start"))    { cl.show(cards, "panelStart");    }
        else if (name.equals("download")) { cl.show(cards, "panelDownload"); }
        else if (name.equals("finish"))   { cl.show(cards, "panelFinish");   }
        else if (name.equals("tryagain")) { cl.show(cards, "panelTryAgain"); }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Start panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes start panel
     * @param szDownload Size of download
     */
    public void startPanelInitialize(Long szDownload) {
        panelStart.initialize(szDownload);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Download panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resets download panel
     */
    public void downloadPanelReset() {
        panelDownload.reset();
    }

    /**
     * Updates download progress
     * @param value Progress value
     * @param description Progress description
     */
    public void downloadPanelUpdateProgress(int value, String description) {
        panelDownload.updateProgress(value, description);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Finish panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes finish panel
     * @param dirname Download directory
     * @param message Result message
     */
    public void finishPanelInitialize(String dirname, String message) {
        panelFinish.initialize(dirname, message);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Counter panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes counter panel
     * @param value Counter value
     */
    public void counterPanelStart(int value) {
        panelCounter.start(value);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Try Again panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes try again panel
     * @param error Error string
     */
    public void tryAgainPanelInitialize(String error) {
        panelTryAgain.initialize(error);
    }
}