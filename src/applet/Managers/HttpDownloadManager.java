package Applet.Managers;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;
import Applet.Threads.ManagerThread;
import Encoder.Decoder;
import Encoder.ProgressCallback;
import Encoder.fs.Common;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.JOptionPane;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpDownloadManager {

    private Context context;                        // Context
    private ProgressCallback progressCallback;      // Progress callback
    private ManagerThread thread;                   // Thread
    private String urlSource;                       // Source URL
    private String tmpFile;                         // Temporary file name
    private String password;                        // Password
    private HttpURLConnection connection = null;    // Connection
    private InputStream reader;                     // Input stream
    private FileOutputStream writer;                // Output stream
    private int szDownload;                         // Size of download

    /**
     * Constructor
     * @param context Applet context
     * @param url Source url
     * @param tmpFile Temporary file name
     * @param password Password
     */
    public HttpDownloadManager(Context context, ProgressCallback progressCallback, ManagerThread thread,
                               String url, String tmpFile, String password) {
        this.context = context;
        this.progressCallback = progressCallback;
        this.thread = thread;
        this.urlSource = url;
        this.tmpFile = tmpFile;
        this.password = password;
    }

    /**
     * @return Target file name
     * @throws IOException
     */
    public String run() throws Exception {
        open(urlSource);
        szDownload = getDownloadSize();
        openStreams();
        download();
        return Decoder.fileName();
    }

    /**
     * @param url Source URL
     * @throws IOException
     */
    public void open(String url) throws IOException {
        connection = (HttpURLConnection) new URL(url + "?" + System.currentTimeMillis()).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Connection", "keep-alive");

        int responseCode = connection.getResponseCode();
        if (responseCode >= 400) {
            InputStreamReader in = new InputStreamReader(connection.getErrorStream());
            BufferedReader buff = new BufferedReader(in);
            String line;
            String text = "";
            do {
                line = buff.readLine();
                if (line != null) { text += line + "\n"; }
            } while (line != null);

            connection.disconnect();

            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(text);
                JSONObject jsonObject  = (JSONObject) obj;
                String err = (String) jsonObject.get("text");
                throw new IOException(err);
            } catch (ParseException e) {
                throw new IOException("Error");
            }
        }
    }

    /**
     * Disconnect connection
     */
    public void disconnect() {
        if (!this.isConnected()) return;
        connection.disconnect();
        connection = null;
    }

    /**
     * @return Download size
     * @throws Exception
     */
    public int getDownloadSize() throws Exception {
        if (!this.isConnected()) throw new Exception("There is no connection");
        return connection.getContentLength();
    }

    /**
     * @throws IOException
     */
    public void openStreams() throws IOException {
        try {
            reader = connection.getInputStream();
        } catch(IOException e) {
            int code = connection.getResponseCode();
            throw new IOException("Error " + String.valueOf(code));
        }
        writer = new FileOutputStream(tmpFile);
    }

    /**
     * @return Connection status
     */
    public boolean isConnected() {
        return (null != connection);
    }

    /**
     * @throws IOException
     */
    public void download() throws Exception {
        try {
            Decoder.initialize(reader, writer, szDownload, password, progressCallback);
            Decoder.updateHeader();
            while (0 < Decoder.updateData()) {
                while (waiting()) {}
            }
            Decoder.updateHMAC();
            Decoder.close();
            disconnect();
        } catch (Exception e) {
            Decoder.close();
            disconnect();
            Common.deleteFile(tmpFile);
            throw new Exception(e.getMessage());
        }
    }

    /**
     * @return Waiting status
     * @throws InterruptedException
     * @throws IOException
     */
    private boolean waiting() throws InterruptedException, IOException {

        // While download paused, wait...
        while (thread.isPaused && !thread.isCanceled) {
            Thread.sleep(100);
            Thread.yield();
        }

        // If download canceled, then exit
        if (thread.isCanceled) {
            thread.isCanceled = false;
            if (thread.isForced) {
                Decoder.close();
                throw new IOException(context.getPolicy().getI18nString("download_int"));
            } else {
                int response = UiDialogHelper.showConfirmMessageBox(context,
                        context.getPolicy().getI18nString("dlg_cancel_mess"),
                        context.getPolicy().getI18nString("dlg_cancel_title"),
                        JOptionPane.YES_NO_OPTION);
                if (JOptionPane.YES_OPTION == response) {
                    throw new IOException(context.getPolicy().getI18nString("download_int"));
                } else {
                    return true;
                }
            }
        }
        return false;
    }
}