package Applet;

import Applet.Handlers.I18nHandler;
import Applet.Helpers.ParamsHelper;

public class Policy {

    private Context context;    // Context
    private I18nHandler i18n;   // Internationalization handler
    private String url;         // URL
    private String pass;        // Pass
    private String ticket;      // Ticket
    private Long downloadSize;   // Download size
    private int delay;          // Delay time

    private static final String PARAM_URL = "url";
    // private static final String PARAM_URL_DEFAULT = "http://31.204.128.90:8080";
    private static final String PARAM_URL_DEFAULT = "";

    private static final String PARAM_PASS = "pass";
    private static final String PARAM_PASS_DEFAULT = "";

    private static final String PARAM_TICKET = "ticket";
    // private static final String PARAM_TICKET_DEFAULT = "223281e6475feb2b3050411c05b372f4236d0a6e8ba2339e92e50c26a998866e";
    private static final String PARAM_TICKET_DEFAULT = "";

    /**
     * Constructor
     * @param context Context
     */
    public Policy(Context context) {
        this.context = context;
        i18n = new I18nHandler();
        parseAppletParameters();
    }

    /**
     * Parses applet parameters
     */
    private void parseAppletParameters() {
        url = ParamsHelper.getParameter(context, PARAM_URL, PARAM_URL_DEFAULT);
        pass = ParamsHelper.getParameter(context, PARAM_PASS, PARAM_PASS_DEFAULT);
        ticket = ParamsHelper.getParameter(context, PARAM_TICKET, PARAM_TICKET_DEFAULT);
    }

    /**
     * Returns localized string
     * @param key String key
     * @return Localized string
     */
    public String getI18nString(String key) {
        return i18n.getString(key);
    }

    /**
     * @return Url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url URL
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @return Ticket
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * @return Download Size
     */
    public Long getDownloadSize() {
        return downloadSize;
    }

    /**
     * @param downloadSize Download Size
     */
    public void setDownloadSize(Long downloadSize) {
        this.downloadSize = downloadSize;
    }

    /**
     * @return Delay time
     */
    public int getDelay() {
        return delay;
    }

    /**
     * @param delay Delay time
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }
}