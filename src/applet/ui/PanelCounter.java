package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Font;
import java.util.TimerTask;

public class PanelCounter extends JPanel {

    private Context context;                // Context
    private PanelCounterCallback callback;  // Counter callback
    private JLabel lblCounter;              // UI: Timer
    private java.util.Timer timer;          // Timer

    /**
     * Constructor
     * @param context Context
     * @param callback Counter callback
     */
    public PanelCounter(Context context, PanelCounterCallback callback) {
        this.context = context;
        this.callback = callback;

        timer = new java.util.Timer();

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/PreLoaders/big.gif"));
        lblCounter = new JLabel();
        lblCounter.setText("24");
        lblCounter.setIcon(icon);
        lblCounter.setHorizontalAlignment(JLabel.CENTER);
        lblCounter.setHorizontalTextPosition(JLabel.CENTER);
        lblCounter.setFont(new Font("Sans", Font.PLAIN, 24));

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(lblCounter);
        bh.add(Box.createGlue());
        bv.add(bh, BorderLayout.CENTER);
        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * Starts counter
     * @param value Counter value
     */
    public void start(int value) {
        update(value);
    }

    /**
     * Updates counter value
     */
    private void update(int counterValue) {
        lblCounter.setText(String.valueOf(counterValue));
        scheduleTimer(counterValue);
    }

    /**
     * Schedule timer function
     */
    private void scheduleTimer(final int counterValue) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() { timerExecute(counterValue); }
        }, 1000);
    }

    /**
     * Timer execution function
     */
    public void timerExecute(int counterValue) {
        if (0 < counterValue--) { update(counterValue); return; }

        // If counter completed, call callback...
        callback.onCounterCompleted();
    }
}