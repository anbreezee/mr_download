package Applet.UI;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class PanelFinish extends JPanel implements ActionListener {

    private Context context;        // Context
    private JLabel lblComplete;     // UI: Complete label
    private JButton btnOpenFolder;  // UI: Show folder button
    private String dirname;         // Target directory

    /**
     * Constructor
     * @param context Context
     */
    public PanelFinish(Context context) {
        this.context = context;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/MessageIcons/icon_finish.png"));
        lblComplete = new JLabel();
        lblComplete.setText("Download complete.");
        lblComplete.setIcon(icon);
        lblComplete.setHorizontalAlignment(JLabel.CENTER);

        final ImageIcon iconOpenFolder = new ImageIcon(getClass().getResource("/Images/Buttons/btn_open.png"));
        btnOpenFolder = new JButton(iconOpenFolder);
        btnOpenFolder.setBorderPainted(false);
        btnOpenFolder.setContentAreaFilled(false);
        btnOpenFolder.setFocusPainted(false);
        btnOpenFolder.addActionListener(this);
        btnOpenFolder.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(lblComplete);
        bh.add(btnOpenFolder);
        bh.add(Box.createGlue());
        bv.add(bh, BorderLayout.CENTER);
        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * Initialization function
     * @param dirname Target directory path
     * @param message Complete message
     */
    public void initialize(String dirname, String message) {
        lblComplete.setText(message);
        this.dirname = dirname;
    }

    /**
     * Handler for actions
     * @param ae Action event
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == btnOpenFolder) {
            try {
                Desktop.getDesktop().open(new File(dirname));
            } catch (IOException e) {
                UiDialogHelper.showErrorMessageBox(context, e.getMessage());
            }
        }
    }
}