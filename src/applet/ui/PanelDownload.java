package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.BorderFactory;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelDownload extends JPanel implements ActionListener {

    private Context context;                    // Context
    private PanelDownloadCallback callback;     // Download callback
    private JProgressBar pbDownload;            // UI: ProgressBar
    // private JButton btnPause;                   // UI: Pause button
    // private JButton btnResume;                  // UI: Resume button
    private JButton btnCancel;                  // UI: Cancel button
    private JLabel lblDownload;                 // UI: Progress label
    private String currentProgressText = "";    // Progress text

    /**
     * Constructor
     * @param context Applet context
     */
    public PanelDownload(Context context, PanelDownloadCallback callback) {
        this.context = context;
        this.callback = callback;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        // final ImageIcon iconPause = new ImageIcon(getClass().getResource("/Images/Icons/icon_pause.png"));
        // final ImageIcon iconResume = new ImageIcon(getClass().getResource("/Images/Icons/icon_resume.png"));
        final ImageIcon iconCancel = new ImageIcon(getClass().getResource("/Images/Icons/icon_cancel.png"));

        pbDownload = new JProgressBar(JProgressBar.HORIZONTAL, 0,  100);
        pbDownload.setValue(0);
        pbDownload.setIndeterminate(true);

        /*
        btnPause = new JButton(iconPause);
        btnPause.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        btnPause.setBorderPainted(false);
        btnPause.setContentAreaFilled(false);
        btnPause.setFocusPainted(false);
        btnPause.addActionListener(this);
        btnPause.setSize(16, 16);
        btnPause.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        */

        /*
        btnResume = new JButton(iconResume);
        btnResume.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        btnResume.setBorderPainted(false);
        btnResume.setContentAreaFilled(false);
        btnResume.setFocusPainted(false);
        btnResume.addActionListener(this);
        btnResume.setSize(16, 16);
        btnResume.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        */

        btnCancel = new JButton(iconCancel);
        btnCancel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        btnCancel.setBorderPainted(false);
        btnCancel.setContentAreaFilled(false);
        btnCancel.setFocusPainted(false);
        btnCancel.addActionListener(this);
        btnCancel.setSize(16, 16);
        btnCancel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        lblDownload = new JLabel();
        lblDownload.setText("Initializing...");

        Box bh = Box.createHorizontalBox();
        bh.add(pbDownload);
        // bh.add(btnPause);
        // bh.add(btnResume);
        bh.add(btnCancel);

        Box bhLbl = Box.createHorizontalBox();
        bhLbl.add(lblDownload);
        bhLbl.add(Box.createHorizontalGlue());

        Box bv = Box.createVerticalBox();
        bv.add(Box.createVerticalGlue());
        bv.add(bh);
        bv.add(bhLbl);
        bv.add(Box.createVerticalGlue());

        switchPauseButton(false);

        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * Updates progress bar and description
     * @param value Progress value
     * @param description Progress description
     */
    public void updateProgress(int value, String description) {
        if (0 < value) pbDownload.setValue(value);
        pbDownload.setIndeterminate(0 == value);
        lblDownload.setText(description);
        currentProgressText = description;
    }

    /**
     * Switches pause button
     * @param isPaused Paused state
     */
    private void switchPauseButton(boolean isPaused) {
        // btnPause.setVisible(!isPaused);
        // btnResume.setVisible(isPaused);
    }

    /**
     * Pauses the download process
     */
    private void pause() {
        lblDownload.setText(currentProgressText + " ... " + context.getPolicy().getI18nString("paused"));
        switchPauseButton(true);
        callback.onPauseDownloadClicked();
    }

    /**
     * Resumes the download process
     */
    private void resume() {
        switchPauseButton(false);
        callback.onResumeDownloadClicked();
    }

    /**
     * Cancel the download process
     */
    private void cancel() {
        callback.onCancelDownloadClicked();
    }

    /**
     * Resets the panel
     */
    public void reset() {
        pbDownload.setValue(0);
        pbDownload.setIndeterminate(true);
        lblDownload.setText("");
        switchPauseButton(false);
    }

    /**
     * Handler for actions
     * @param e Action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        /*if (e.getSource() == btnPause)       { pause();  }
        else if (e.getSource() == btnResume) { resume(); }
        else */ if (e.getSource() == btnCancel) { cancel(); }
    }
}