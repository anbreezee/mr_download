package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static Encoder.utils.Format.readableFileSize;

public class PanelStart extends JPanel implements ActionListener {

    private Context context;                // Context
    private PanelStartCallback callback;    // Callback
    private JButton btnDownload;            // UI: Download button
    private JLabel lblSize;                 // UI: Size label

    /**
     * Constructor
     * @param context Context
     */
    public PanelStart(Context context, PanelStartCallback callback) {
        this.context = context;
        this.callback = callback;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/Buttons/btn_download.png"));

        btnDownload = new JButton(icon);
        btnDownload.setBorderPainted(false);
        btnDownload.setContentAreaFilled(false);
        btnDownload.setFocusPainted(false);
        btnDownload.addActionListener(this);
        btnDownload.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        lblSize = new JLabel();
        lblSize.setText("");
        lblSize.setHorizontalAlignment(JLabel.CENTER);

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(btnDownload);
        bh.add(lblSize);
        bh.add(Box.createGlue());
        bv.add(bh);
        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * Initialization function
     * @param szDownload Size of download
     */
    public void initialize(Long szDownload) {
        lblSize.setText(
                String.format(context.getPolicy().getI18nString("file_size"), readableFileSize(szDownload)));
    }

    /**
     * Handler for actions
     * @param ae Action event
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == btnDownload) {
            callback.onStartDownloadClicked();
        }
    }
}
