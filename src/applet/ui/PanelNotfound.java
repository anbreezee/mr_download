package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;

public class PanelNotFound extends JPanel {

    private Context context;    // Context
    private JLabel lbl403;      // UI: Label

    /**
     * Constructor
     * @param context Context
     */
    public PanelNotFound(Context context) {
        this.context = context;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/Screens/not-found.png"));
        lbl403 = new JLabel();
        lbl403.setIcon(icon);
        lbl403.setHorizontalAlignment(JLabel.CENTER);

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(lbl403);
        bh.add(Box.createGlue());
        bv.add(bh, BorderLayout.CENTER);
        this.add(bv, BorderLayout.CENTER);
    }
}