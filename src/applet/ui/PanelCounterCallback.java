package Applet.UI;

public interface PanelCounterCallback {

    // Calls when counter completed
    public void onCounterCompleted();
}
