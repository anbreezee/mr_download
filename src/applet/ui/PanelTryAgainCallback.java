package Applet.UI;

public interface PanelTryAgainCallback {
    public void onTryAgainClicked();
}
