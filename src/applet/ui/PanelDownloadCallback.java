package Applet.UI;

public interface PanelDownloadCallback {
    public void onPauseDownloadClicked();
    public void onResumeDownloadClicked();
    public void onCancelDownloadClicked();
}
