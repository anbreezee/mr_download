package Applet.Threads;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class UrlTestThread extends Thread {

    private Context context;                    // Context
    private UrlTestThreadCallback callback;     // Callback
    private String url;                         // Test URL

    /**
     * Constructor
     * @param context Applet context
     */
    public UrlTestThread(Context context, UrlTestThreadCallback callback, String url) {
        super("UrlTestThread");
        this.context = context;
        this.callback = callback;
        this.url = url;
    }

    /** Run function */
    @Override
    public void run() {
        try {
            int szDownload = testUrl(url);
            if (0 >= szDownload) callback.onNotFound();     // Not found
            else callback.onGrantsSuccess(szDownload);      // Grants success
        } catch (IOException e) {
            UiDialogHelper.showErrorMessageBox(context, e.getMessage());
        }
    }

    /**
     * @param url Target url
     * @return Download size
     * @throws java.io.IOException
     */
    public int testUrl(String url) throws IOException {
        URLConnection connection = new URL(url + "?" + System.currentTimeMillis()).openConnection();
        int szDownload = connection.getContentLength();
        ((HttpURLConnection) connection).disconnect();
        return szDownload;
    }
}
