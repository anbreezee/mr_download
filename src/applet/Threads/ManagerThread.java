package Applet.Threads;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;
import Applet.Managers.HttpDownloadManager;
import Applet.Helpers.UrlHelper;
import Encoder.fs.Common;

import javax.swing.JFileChooser;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class ManagerThread extends Thread {

    private Context context;                        // Context
    private ManagerThreadCallback callback;         // Callback
    private JFileChooser fc = null;                 // File chooser
    public volatile boolean isPaused = false;       // Flag: paused
    public volatile boolean isCanceled = false;     // Flag: canceled
    public volatile boolean isForced = false;     // Flag: forced

    /**
     * Constructor
     * @param context Applet context
     * @param callback Callback
     */
    public ManagerThread(Context context, ManagerThreadCallback callback) {
        super("ManagerThread");
        this.context = context;
        this.callback = callback;
        fc = new JFileChooser();
    }

    /**
     * Run function
     */
    @Override
    public void run() {
        String url;
        String ticket;
        String password;
        String targetFolder;
        String tmpFile;

        // We must test params
        url = context.getPolicy().getUrl();
        if (url.equals("")) {
            // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("params_url_err"));
            reset();
            callback.onError(context.getPolicy().getI18nString("params_url_err"));
            return;
        }

        ticket = context.getPolicy().getTicket();
        if (ticket.equals("")) {
            // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("params_ticket_err"));
            reset();
            callback.onError(context.getPolicy().getI18nString("params_ticket_err"));
            return;
        }

        // Construct download url
        url = UrlHelper.constructDlUrl(context);

        // Firstly, get password or user should enter the password
        password = context.getPolicy().getPass();
        if (password.equals("")) {
            password = promptPassword();
        } else {
            try {
                password = UrlHelper.decodeUrlSafe(password);
            } catch (Exception e) {
                //UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("params_pass_err"));
                reset();
                callback.onError(context.getPolicy().getI18nString("params_pass_err"));
                return;
            }
        }
        if (password == null || password.equals("")) {
            UiDialogHelper.showWarningMessageBox(context,
                    context.getPolicy().getI18nString("dlg_code_err"),
                    context.getPolicy().getI18nString("dlg_code_title"));
            reset();
            return;
        }

        // Secondly, user should select folder to save to
        try {
            targetFolder = browseDirectory((JComponent) context.getApplet().getContentPane());
        } catch (IOException e) {
            // UiDialogHelper.showErrorMessageBox(context, e.getMessage());
            reset();
            callback.onError(e.getMessage());
            return;
        }
        if (null == targetFolder || !(new File(targetFolder).isDirectory())) {
            UiDialogHelper.showWarningMessageBox(context,
                    context.getPolicy().getI18nString("dlg_folder_err"),
                    context.getPolicy().getI18nString("dlg_folder_title"));
            reset();
            return;
        }

        // Thirdly, get temp filename
        tmpFile = this.findTempFile(targetFolder);
        if (null == tmpFile) {
            // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("dlg_folder_err_other"));
            reset();
            callback.onError(context.getPolicy().getI18nString("dlg_folder_err_other"));
            return;
        }

        // And then we are starting the download
        try {
            String filename = downloadUrl(url, tmpFile, password);

            File file = new File(targetFolder + filename);
            if (file.exists()) {
                int response = UiDialogHelper.showConfirmMessageBox(context,
                        String.format(context.getPolicy().getI18nString("file_exists"), filename),
                        context.getPolicy().getI18nString("overwrite"),
                        JOptionPane.YES_NO_CANCEL_OPTION);
                if (JOptionPane.YES_OPTION == response) {
                    Common.deleteFile(targetFolder + filename);
                    Common.renameFile(tmpFile, targetFolder + filename);
                } else if (JOptionPane.NO_OPTION == response) {
                    filename = findAltFilename(filename, targetFolder);
                    if (null == filename) {
                        //UiDialogHelper.showErrorMessageBox(context,
                        //        context.getPolicy().getI18nString("download_failed"));
                        reset();
                        callback.onError(context.getPolicy().getI18nString("download_failed"));
                        return;
                    }
                    Common.renameFile(tmpFile, targetFolder + filename);
                } else if (JOptionPane.CANCEL_OPTION == response) {
                    Common.deleteFile(tmpFile);
                    UiDialogHelper.showInterruptedMessageBox(context,
                            context.getPolicy().getI18nString("download_int"));
                    reset();
                    return;
                }
            } else {
                Common.renameFile(tmpFile, targetFolder + filename);
            }
            finished(targetFolder, filename);
        } catch (Exception e) {
            // UiDialogHelper.showErrorMessageBox(context,
            //        String.format("%s%n%s", context.getPolicy().getI18nString("download_failed"), e.getMessage()));
            reset();
            callback.onError(e.getMessage());
        }
    }

    /**
     * Resets the download
     */
    private void reset() {
        callback.onResetDownload();
    }

    /**
     * Called when download is finished
     */
    private void finished(String targetFolder, String filename) {
        callback.onFinishDownload(targetFolder, filename);
    }

    /**
     * Pauses the download process
     */
    public void pauseDownload() {
        isPaused = true;
        isCanceled = false;
    }

    /**
     * Resumes the download process
     */
    public void resumeDownload() {
        isPaused = false;
        isCanceled = false;
    }

    /**
     * Cancel the download process
     */
    public void cancelDownload(boolean force) {
        isForced = force;
        isCanceled = true;
    }

    /**
     * Prompts password from user
     * @return Password or null, if dialog canceled
     */
    private String promptPassword() {
        JFrame frame = new JFrame("Password Dialog");
        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/MessageIcons/icon_password.png"));
        return (String) JOptionPane.showInputDialog(frame,
                context.getPolicy().getI18nString("dlg_code_mess"),
                context.getPolicy().getI18nString("dlg_code_title"),
                JOptionPane.WARNING_MESSAGE, icon, null, null);
    }

    /**
     * Starts "Browse for directory" dialog
     * @param owner Parent component
     * @return Path for selected directory or null, if dialog canceled
     */
    private String browseDirectory(JComponent owner) throws IOException {
        fc.setDialogTitle(context.getPolicy().getI18nString("dlg_folder_title"));
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setApproveButtonText("Select");
        int returnVal = fc.showOpenDialog(owner);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String directory = file.getCanonicalPath();
            if (!directory.endsWith(File.separator)) {
                directory += File.separator;
            }
            return directory;
        }
        return null;
    }

    /**
     * Returns path to temporary file
     * @param dirname Target folder
     * @return path to temporary file
     */
    private String findTempFile(String dirname) {
        File dir = new File(dirname);
        if (!dir.isDirectory()) {
            return null;
        }
        cleanFolder(dirname);
        String filename;
        for (int i = 1; i < 10000; i++) {
            filename = "mr." + String.format("%04d", i) + ".part";
            File file = new File(dirname + filename);
            if (!file.exists()) {
                return dirname + filename;
            }
        }
        return null;
    }

    /**
     * Returns alternate target file name
     * @param filename Target filename
     * @param dirname Target folder
     * @return alternate target file name
     */
    private String findAltFilename(String filename, String dirname) {
        String[] tokens = filename.split("\\.(?=[^\\.]+$)");
        String altname;
        for (int i = 1; i < 10000; i++) {
            altname = tokens[0] + " (" + i + ")." + tokens[1];
            File file = new File(dirname + altname);
            if (!file.exists()) {
                return altname;
            }
        }
        return null;
    }

    /**
     * Removes old temporary files in target folder
     * @param dirname Target folder
     */
    private void cleanFolder(String dirname) {
        File dir = new File(dirname);
        File [] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("mr.") && name.endsWith(".part")
                        && (0 < Integer.parseInt(name.substring(3, 7)));
            }
        });
        long purgeTime = System.currentTimeMillis() - (4 * 60 * 60 * 1000);
        for (File file : files) {
            if (file.lastModified() < purgeTime) {
                if (!file.delete()) {
                    // No implementation required
                }
            }
        }
    }

    /**
     * Starts download process
     * @param url Source url
     * @param tmpFile Name of temporary file
     * @param password Password for current download
     * @return Real filename of download
     * @throws IOException
     */
    private String downloadUrl(String url, String tmpFile, String password) throws Exception {
        HttpDownloadManager connect = new HttpDownloadManager(context, context, this, url, tmpFile, password);
        return connect.run();
    }
}