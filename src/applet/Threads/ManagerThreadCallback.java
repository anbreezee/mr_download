package Applet.Threads;

public interface ManagerThreadCallback {
    public void onResetDownload();
    public void onFinishDownload(String targetFolder, String filename);
    public void onError(String err);
}
