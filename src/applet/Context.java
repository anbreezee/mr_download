package Applet;

import Applet.Handlers.CardsHandler;
import Applet.Threads.*;
import Applet.UI.*;
import Encoder.ProgressCallback;
import netscape.javascript.JSObject;

import javax.swing.*;

public class Context implements UrlTestThreadCallback, ManagerThreadCallback, ArrowThreadCallback,
                                ProgressCallback, PanelCounterCallback, PanelStartCallback, PanelDownloadCallback,
                                PanelTryAgainCallback {

    private Applet applet;                  // Applet
    private Policy policy;                  // Policy
    private CardsHandler cardsHandler;      // Cards handler
    private ManagerThread downloadThread;   // Download Manager thread
    private ArrowThread arrowThread;        // Arrows!

    /**
     * Constructor
     * @param applet Applet
     */
    public Context(Applet applet) {
        this.applet = applet;
        policy = new Policy(this);
        cardsHandler = new CardsHandler(this);
    }

    /**
     * @return Applet
     */
    public Applet getApplet() {
        return applet;
    }

    /**
     * return Policy
     */
    public Policy getPolicy() {
        return policy;
    }

    /**
     * Constructs user interface
     */
    public void constructUI() {
        cardsHandler.ConstructUI();
    }

    /**
     * Starts logic
     */
    public void start() {
        arrowThread = new ArrowThread(this, Arrow.Direction.DOWN);
        arrowThread.start();
        _wait();
        testTargetUrl();
    }

    /**
     * Tests target URL
     */
    public void testTargetUrl() {
        (new UrlTestThread(this, this)).start();
    }

    /**
     * Updates download progress
     * @param value Progress value
     * @param description Progress description
     */
    public void updateProgress(final int value, final String description) {

        // Return, if paused...
        if (null != downloadThread && downloadThread.isPaused) {
            return;
        }

        // Update progress
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                cardsHandler.downloadPanelUpdateProgress(value, description);
            }
        });
    }

    public void stopArrows() {
        arrowThread.stopArrows();
    }

    public void cancelDownload() {
        downloadThread.cancelDownload(true);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CALLBACKS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onError(String err) {
        stopArrows();
        if (err.equals("Not found")) {
            _notfound();
            return;
        }
        if (err.equals("No ticket")) {
            err = "There is no ticket for download now. Please try again.";
        }
        _error(err);
    }

    /**
     * If grants success
     */
    @Override
    public void onGrantsSuccess(Long size, int delay) {
        policy.setDownloadSize(size);
        policy.setDelay(delay);
        if (delay == 0) {
            _start(policy.getDownloadSize());
        } else {
            _counter();
        }
    }

    /**
     * When download reset
     */
    @Override
    public void onResetDownload() {
        _reset();
    }

    /**
     * When download finished
     */
    @Override
    public void onFinishDownload(String targetFolder, String filename) {
        _finish(targetFolder, filename);
    }

    /**
     * Starts the download process
     */
    @Override
    public void onStartDownloadClicked() {
        stopArrows();
        _download();
        downloadThread = new ManagerThread(this, this);
        downloadThread.start();
    }

    /**
     * Pauses the download process
     */
    @Override
    public void onPauseDownloadClicked() {
        downloadThread.pauseDownload();
    }

    /**
     * Resumes the download process
     */
    @Override
    public void onResumeDownloadClicked() {
        downloadThread.resumeDownload();
    }

    /**
     * Cancel the download process
     */
    @Override
    public void onCancelDownloadClicked() {
        downloadThread.cancelDownload(false);
    }

    /**
     * Counter callback
     */
    @Override
    public void onCounterCompleted() {
        _start(policy.getDownloadSize());
    }

    @Override
    public void onArrowTick() {
        this.getApplet().repaintArrows(this.arrowThread);
    }

    @Override
    public void onTryAgainClicked() {
        // Reload page
        try {
            JSObject win = JSObject.getWindow(applet);
            win.call("reloadApplet", null);
        } catch (Exception ex) {
            // OOPS!
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GRAPH NODES
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows counter panel
     */
    public void _counter() {
        cardsHandler.showCard("counter");
        cardsHandler.counterPanelStart(policy.getDelay());
    }

    /**
     * Shows error panel
     * @param err Error string
     */
    public void _error(String err) {
        cardsHandler.tryAgainPanelInitialize(err);
        cardsHandler.showCard("tryagain");
    }


    /**
     * Shows not found panel
     */
    public void _notfound() {
        cardsHandler.showCard("notfound");
    }

    /**
     * Shows start download panel
     * @param szDownload Size of download
     */
    public void _start(Long szDownload) {
        cardsHandler.startPanelInitialize(szDownload);
        cardsHandler.showCard("start");
    }

    /**
     * Shows start download panel
     */
    public void _reset() {
        cardsHandler.showCard("start");
    }

    /**
     * Show finish download panel
     * @param targetFolder Target folder
     * @param filename Target filename
     */
    public void _finish(String targetFolder, String filename) {
        cardsHandler.finishPanelInitialize(targetFolder,
                String.format(policy.getI18nString("download_success"), filename));
        cardsHandler.showCard("finish");
    }

    /**
     * Show download progress panel
     */
    public void _download() {
        cardsHandler.downloadPanelReset();
        cardsHandler.showCard("download");
        cardsHandler.downloadPanelUpdateProgress(0, "Initializing...");
    }

    /**
     * Show wait panel
     */
    public void _wait() {
        cardsHandler.showCard("wait");
    }
}