package Applet.Helpers;

import Applet.Context;

import javax.swing.JOptionPane;

public class UiDialogHelper {

    /**
     * Shows Success Message Box
     * @param context Context
     * @param message Success message text
     */
    public static void showSuccessMessageBox(Context context, String message) {
        String title = context.getPolicy().getI18nString("success");
        JOptionPane.showMessageDialog(context.getApplet().getContentPane(),
                message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Shows Error Message Box
     * @param context Context
     * @param message Error message text
     */
    public static void showErrorMessageBox(Context context, String message) {
        String title = context.getPolicy().getI18nString("err");
        JOptionPane.showMessageDialog(context.getApplet().getContentPane(),
                message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows Interrupted Message Box
     * @param context Context
     * @param message Message text
     */
    public static void showInterruptedMessageBox(Context context, String message) {
        String title = context.getPolicy().getI18nString("int");
        JOptionPane.showMessageDialog(context.getApplet().getContentPane(),
                message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows Warning Message Box
     * @param context Context
     * @param message Message text
     * @param title Message title
     */
    public static void showWarningMessageBox(Context context, String message, String title) {
        JOptionPane.showMessageDialog(context.getApplet().getContentPane(),
                message, title, JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Shows Confirm Dialog
     * @param context Context
     * @param message Message text
     * @param title Message title
     * @param options Dialog options
     * @return User choice
     */
    public static int showConfirmMessageBox(Context context, String message, String title, int options) {
        return JOptionPane.showConfirmDialog(context.getApplet().getContentPane(), message, title,
                options, JOptionPane.INFORMATION_MESSAGE);
    }
}